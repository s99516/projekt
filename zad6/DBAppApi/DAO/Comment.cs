﻿using System;
using System.Collections.Generic;

namespace DBAppApi.DAO
{
    public class Comment
    {
        public Comment()
        {
            SubComments = new List<Comment>();
        }

        public int CommentId { get; set; }
        public int? ParentCommentId { get; set; }
        public int FilmId { get; set; }
        public int UserId { get; set; }
        public string CommentBody { get; set; }
        public DateTime CommentDate { get; set; }
        public bool IsActiveComment { get; set; }
        public bool IsBannedComment { get; set; }

        public virtual Film Film { get; set; }
        public virtual User User { get; set; }
        public virtual Comment ParentComment { get; set; }
        public virtual ICollection<Comment> SubComments { get; set; }
    }
}