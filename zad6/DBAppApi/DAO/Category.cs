﻿using System.Collections.Generic;

namespace DBAppApi.DAO
{
    public class Category
    {
        public Category()
        {
            FilmCategories = new List<FilmCategory>();
        }

        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public string ImageHref { get; set; }

        public virtual ICollection<FilmCategory> FilmCategories { get; set; }
    }
}