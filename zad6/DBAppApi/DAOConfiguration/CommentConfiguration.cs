﻿using DBAppApi.DAO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DBAppApi.DAOConfiguration
{
    public class CommentConfiguration : IEntityTypeConfiguration<Comment>
    {
        public void Configure(EntityTypeBuilder<Comment> builder)
        {
            builder.Property(c => c.CommentBody).IsRequired();

            //ustawienie propercji typu Boolean jako kolumna typu tinyint
            builder.Property(c => c.IsActiveComment).HasColumnType("tinyint(1)");
            builder.Property(c => c.IsBannedComment).HasColumnType("tinyint(1)");

            //konfiguracja zależności jeden do wielu 
            builder.HasOne(x => x.Film)
                .WithMany(x => x.Comments)
                // Konfiguruje sposób, w jaki operacja usuwania jest stosowana
                // do jednostek zależnych w relacji, gdy jednostka główna zostanie
                // usunięta lub relacja zostanie zerwana.
                .OnDelete(DeleteBehavior.Restrict)
                //ustawienie klucza głównego
                .HasForeignKey(x => x.FilmId);

            builder.HasOne(x => x.User)
                .WithMany(x => x.Comments)
                .OnDelete(DeleteBehavior.Restrict)
                .HasForeignKey(x => x.UserId);

            builder.HasOne(x => x.ParentComment)
                .WithMany(x => x.SubComments)
                .OnDelete(DeleteBehavior.Restrict)
                .HasForeignKey(x => x.ParentCommentId);

            builder.ToTable("Comment");
        }
    }
}