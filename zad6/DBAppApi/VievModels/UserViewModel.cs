﻿using System;
using DBAppApi.Enums;

namespace DBAppApi.VievModels
{
    public class UserViewModel
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public DateTime RegistrationDate { get; set; }
        public DateTime EditionDate { get; set; }
        public DateTime UserInfoEditionDate { get; set; }
        public Gender Gender { get; set; }
        public DateTime BirthDate { get; set; }
        //public bool IsBannedUser { get; set; }
        //public bool IsActiveUser { get; set; }
        public string AccountDescription { get; set; }
        //public string AccountPrivateHref { get; set; }
        public string IconHref { get; set; }
        public string ThumbnailHref { get; set; }
    }
}