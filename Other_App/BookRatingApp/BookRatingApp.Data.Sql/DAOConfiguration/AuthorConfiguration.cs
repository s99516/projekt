﻿using BookRatingApp.Data.Sql.DAO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace BookRatingApp.Data.Sql.DAOConfiguration
{
    public class AuthorConfiguration : IEntityTypeConfiguration<Author>
    {
        public void Configure(EntityTypeBuilder<Author> builder)
        {
            // //required properties
            // builder.Property(x => x.Name).IsRequired();
            // builder.Property(x => x.Surname).IsRequired();
            // builder.Property(x => x.DateOfBirth).IsRequired();
            // //building table
            // builder.ToTable("Author");
        }
    }
}