﻿using BookRatingApp.Data.Sql.DAO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace BookRatingApp.Data.Sql.DAOConfiguration
{
    public class Book_categoryConfiguration : IEntityTypeConfiguration<Book_category>
    {
        public void Configure(EntityTypeBuilder<Book_category> builder)
        {
            //foreign keys
            builder.HasOne(x => x.Book)
                .WithMany(x => x.BookCategories)
                .OnDelete(DeleteBehavior.Restrict)
                .HasForeignKey(x => x.Book_id);
            builder.HasOne(x => x.Category)
                .WithMany(x => x.BookCategories)
                .OnDelete(DeleteBehavior.Restrict)
                .HasForeignKey(x => x.Category_id);
            //building table
            builder.ToTable("BookCategory");
        }
    }
}