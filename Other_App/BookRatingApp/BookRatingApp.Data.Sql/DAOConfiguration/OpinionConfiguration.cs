﻿using BookRatingApp.Data.Sql.DAO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace BookRatingApp.Data.Sql.DAOConfiguration
{
    public class OpinionConfiguration : IEntityTypeConfiguration<Opinion>
    {
        public void Configure(EntityTypeBuilder<Opinion> builder)
        {
            //required properties
            builder.Property(x => x.Oponion_body).IsRequired();
            builder.Property(x => x.Opinion_date).IsRequired();
            //boolean type properties
            builder.Property(x => x.IsActiveComment).HasColumnType("tinyint(1)");
            //foreign keys
            builder.HasOne(x => x.Book)
                .WithMany(x => x.Opinions)
                .OnDelete(DeleteBehavior.Restrict)
                .HasForeignKey(x => x.Book_id);
            builder.HasOne(x => x.User)
                .WithMany(x => x.Opinions)
                .OnDelete(DeleteBehavior.Restrict)
                .HasForeignKey(x => x.User_id);
            // builder.HasOne(x => x.ParentOpinion)
            //     .WithMany(x => x.Sub_opinions)
            //     .OnDelete(DeleteBehavior.Restrict)
            //     .HasForeignKey(x => x.Parent_opinion_id);
            //building table
            builder.ToTable("Opinion");
        }
    }
}