﻿using BookRatingApp.Data.Sql.DAO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace BookRatingApp.Data.Sql.DAOConfiguration
{
    public class CategoryConfiguration : IEntityTypeConfiguration<Category>
    {
        public void Configure(EntityTypeBuilder<Category> builder)
        {
            //reqired properties
            builder.Property(x => x.Category_name).IsRequired();
            //building table
            builder.ToTable("Category");
        }
    }
}