﻿using BookRatingApp.Data.Sql.DAO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace BookRatingApp.Data.Sql.DAOConfiguration
{
    public class BookConfiguration : IEntityTypeConfiguration<Book>
    {
        public void Configure(EntityTypeBuilder<Book> builder)
        {
            //required properties
            builder.Property(x => x.Book_title).IsRequired();
            builder.Property(x => x.Page_count).IsRequired();
            builder.Property(x => x.RealeseDate).IsRequired();
            //building table
            builder.ToTable("Book");
        }
    }
}