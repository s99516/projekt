﻿using System.ComponentModel.Design;
using BookRatingApp.Data.Sql.DAO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace BookRatingApp.Data.Sql.DAOConfiguration
{
    public class UserConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            //required properties
            builder.Property(x => x.Nick).IsRequired();
            builder.Property(x => x.Name).IsRequired();
            builder.Property(x=>x.Surname).IsRequired();
            builder.Property(x => x.Date_of_join).IsRequired();
            
            //boolean type properties
            builder.Property(x => x.IsActiveUser).HasColumnType("tinyint(1)");
            
            //building table
            builder.ToTable("User");
        }
    }
}