﻿using System.Collections.Immutable;
using BookRatingApp.Data.Sql.DAO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace BookRatingApp.Data.Sql.DAOConfiguration
{
    public class Book_authorConfiguration : IEntityTypeConfiguration<Book_author>
    {
        public void Configure(EntityTypeBuilder<Book_author> builder)
        {
            //foreign keys
            builder.HasOne(x => x.Author)
                .WithMany(x => x.BookAuthors)
                .OnDelete(DeleteBehavior.Restrict)
                .HasForeignKey(x => x.Author_id);
            builder.HasOne(x => x.Book)
                .WithMany(x => x.BookAuthors)
                .OnDelete(DeleteBehavior.Restrict)
                .HasForeignKey(x => x.Book_id);
            //building table
            builder.ToTable("BookAuthor");
        }
    }
}