﻿using BookRatingApp.Data.Sql.DAO;
using BookRatingApp.Data.Sql.DAOConfiguration;
using Microsoft.EntityFrameworkCore;

namespace BookRatingApp.Data.Sql
{
    public class BookRatingAppDbContext : DbContext
    {
        public BookRatingAppDbContext(DbContextOptions<BookRatingAppDbContext> options) : base(options) {}
        
        public virtual DbSet<User> User { get; set; }
        public virtual DbSet<Book> Book { get; set; }
        
        public virtual DbSet<Author> Author { get; set; }
        public virtual DbSet<Category> Category { get; set; }
        
        public virtual DbSet<Book_author> BookAuthor { get; set; }
        public virtual DbSet<Book_category> BookCategory { get; set; }
        public virtual DbSet<Opinion> Opinion { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfiguration(new UserConfiguration());
            builder.ApplyConfiguration(new BookConfiguration());
            builder.ApplyConfiguration(new AuthorConfiguration());
            builder.ApplyConfiguration(new CategoryConfiguration());
            builder.ApplyConfiguration(new Book_categoryConfiguration());
            builder.ApplyConfiguration(new Book_authorConfiguration());
            builder.ApplyConfiguration(new OpinionConfiguration());
        }
    }
}