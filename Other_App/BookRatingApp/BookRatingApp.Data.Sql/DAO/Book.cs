﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Org.BouncyCastle.Asn1.Crmf;
using Org.BouncyCastle.Utilities.Zlib;

namespace BookRatingApp.Data.Sql.DAO
{
    public class Book
    {
        public Book()
        {
            Opinions = new List<Opinion>();
            BookCategories = new List<Book_category>();
            BookAuthors = new List<Book_author>();
        }
        [Key]
        public int Book_id { get; set; }//this is primary key
        public string Book_title { get; set; }
        public int Page_count { get; set; }
        public DateTime RealeseDate { get; set; }
        
        public virtual ICollection<Opinion> Opinions { get; set; }
        public virtual ICollection<Book_category> BookCategories { get; set; }
        public virtual ICollection<Book_author> BookAuthors { get; set; }
    }
}