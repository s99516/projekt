﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using BookRatingApp.Data.Sql.Enums;

namespace BookRatingApp.Data.Sql.DAO
{
    public class User
    {
        public User()
        {
            Opinions = new List<Opinion>();
        }
        [Key]
        public int User_id { get; set; }//this is primary key
        public string Nick { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public Gender Gender { get; set; }
        public DateTime Date_of_join { get; set; }
        public int Opinions_count { get; set; }
        public bool IsActiveUser { get; set; }

        public virtual ICollection<Opinion> Opinions { get; set; }
    }
}