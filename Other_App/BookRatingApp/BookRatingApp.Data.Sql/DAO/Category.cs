﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BookRatingApp.Data.Sql.DAO
{
    public class Category
    {
        public Category()
        {
            BookCategories = new List<Book_category>();
        }
        [Key]
        public int Category_id { get; set; }//this is primary key
        public string Category_name { get; set; }
        
        public virtual ICollection<Book_category> BookCategories { get; set; }
    }
}