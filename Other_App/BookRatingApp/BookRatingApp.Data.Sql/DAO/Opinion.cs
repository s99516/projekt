﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Org.BouncyCastle.Asn1.Crmf;

namespace BookRatingApp.Data.Sql.DAO
{
    public class Opinion
    {
        public Opinion()
        {
            Sub_opinions = new List<Opinion>();
        }
        [Key]
        public int Opinion_id { get; set; }//this is primary key
        public int User_id { get; set; }
        public int Book_id { get; set; }
        public string Oponion_body { get; set; }
        public DateTime Opinion_date { get; set; }
        public int Parent_opinion_id { get; set; }
        public bool IsActiveComment { get; set; }
        
        
        public virtual User User { get; set; }
        public virtual Opinion ParentOpinion { get; set; }
        public virtual Book Book { get; set; }
        public virtual ICollection<Opinion> Sub_opinions { get; set; }
    }
}