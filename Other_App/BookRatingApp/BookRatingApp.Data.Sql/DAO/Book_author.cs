﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.Design;

namespace BookRatingApp.Data.Sql.DAO
{
    public class Book_author
    {
        [Key]
        public int Book_author_id { get; set; }//this is primary key
        public int Author_id { get; set; }
        public int Book_id { get; set; }
        
        public virtual Author Author { get; set; }
        public virtual Book Book { get; set; }
    }
}