﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BookRatingApp.Data.Sql.DAO
{
    public class Book_category
    {
        [Key]
        public int Book_category_id { get; set; }//this is primary key
        public int Book_id { get; set; }
        public int Category_id { get; set; }
        public virtual Book Book { get; set; }
        public virtual Category Category { get; set; }
    }
}