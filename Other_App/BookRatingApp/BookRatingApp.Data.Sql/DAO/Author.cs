﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BookRatingApp.Data.Sql.DAO
{
    public class Author
    {
        public Author()
        {
            BookAuthors = new List<Book_author>();
        }
        [Key]
        public int Author_id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public DateTime DateOfBirth { get; set; } 
        public DateTime DateOfDeath { get; set; }
        
        public virtual ICollection<Book_author> BookAuthors { get; set; }
    }
}