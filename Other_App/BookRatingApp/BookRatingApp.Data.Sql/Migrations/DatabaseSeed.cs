﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using BookRatingApp.Data.Sql.DAO;
using BookRatingApp.Data.Sql.Enums;

namespace BookRatingApp.Data.Sql.Migrations
{
    public class DatabaseSeed
    {
        private readonly BookRatingAppDbContext _context;

        public DatabaseSeed(BookRatingAppDbContext context)
        {
            _context = context;
        }

        public void Seed()
        {
            var userList = BuildUserList();
            _context.User.AddRange(userList);
            _context.SaveChanges();

            var authorList = BuildAuthorList();
            _context.Author.AddRange(authorList);
            _context.SaveChanges();

            var bookList = BuildBookList();
            _context.Book.AddRange(bookList);
            _context.SaveChanges();

            var bookAuthorList = BuildBookAuthorList(bookList, authorList);
            _context.BookAuthor.AddRange(bookAuthorList);
            _context.SaveChanges();
            
            var categoryList = BuildCategoryList();
            _context.Category.AddRange(categoryList);
            _context.SaveChanges();

            var bookCategoryList = BuildBookCategoryList(bookList, categoryList);
            _context.BookCategory.AddRange(bookCategoryList);
            _context.SaveChanges();
            

            var opinionList = BuildOpinionList(userList, bookList);
            _context.Opinion.AddRange(opinionList);
            _context.SaveChanges();
        }

        private IEnumerable<User> BuildUserList()
        {
            var userList = new List<User>();
            //adding user manually
            var user1 = new User()
            {
                Nick = "MySuperNick",
                Name = "Rafal",
                Surname = "Zajac",
                Gender = 0,
                Date_of_join = new DateTime(2004, 10, 3),
                IsActiveUser = true
            };
            userList.Add(user1);
            
            var user2 = new User()
            {
                Nick = "ExtraNick",
                Name = "Abak",
                Surname = "Abacki",
                Gender = 0,
                Date_of_join = new DateTime(2007, 12, 1),
                IsActiveUser = true
            };
            userList.Add(user2);
            
            var user3 = new User()
            {
                Nick = "ExtraKacperNick",
                Name = "Kacper",
                Surname = "Koza",
                Gender = 0,
                Date_of_join = new DateTime(2010, 12, 11),
                IsActiveUser = true
            };
            userList.Add(user3);

            Random random = new Random();
            var GenderVariable = 0;
            //adding user in loop
            for (int i = 0; i < 17; i++)
            {
                GenderVariable = random.Next(0, 2);
                if (GenderVariable == 0)
                {
                    var loopuser = new User()
                    {
                        Nick = "Nick" + i,
                        Name = "UserName" + (i + 4),
                        Surname = "UserSurname" + (i + 4),
                        Gender = Gender.Male,
                        Date_of_join = DateTime.Now,
                        IsActiveUser = true
                    };
                    userList.Add(loopuser);
                }
                else
                {
                    var loopuser = new User()
                    {
                        Nick = "Nick" + i,
                        Name = "UserName" + (i + 4),
                        Surname = "UserSurname" + (i + 4),
                        Gender = Gender.Female,
                        Date_of_join = DateTime.Now,
                        IsActiveUser = true
                    };
                    userList.Add(loopuser);
                }
            }
            return userList;
        }

        private IEnumerable<Author> BuildAuthorList()
        {
            var authorList = new List<Author>();
            var author1 = new Author()
            {
                Name = "Stephen",
                Surname = "King",
                DateOfBirth = new DateTime(1999, 12, 8),
            };
            authorList.Add(author1);
            
            var author2 = new Author()
            {
                Name = "Jeremy",
                Surname = "Clarkson",
                DateOfBirth = new DateTime(1999, 12, 8),
            };
            authorList.Add(author2);
            
            var author3 = new Author()
            {
                Name = "Sławomir",
                Surname = "Mentzen",
                DateOfBirth = new DateTime(1999, 12, 8),
            };
            authorList.Add(author3);
            
            var author4 = new Author()
            {
                Name = "Jean",
                Surname = "Webster",
                DateOfBirth = new DateTime(1999, 12, 8),
            };
            authorList.Add(author4);
            
            var author5 = new Author()
            {
                Name = "Czesław",
                Surname = "Miłosz",
                DateOfBirth = new DateTime(1999, 12, 8),
            };
            authorList.Add(author5);
            
            var author6 = new Author()
            {
                Name = "Bolesław",
                Surname = "Prus",
                DateOfBirth = new DateTime(1999, 12, 8),
            };
            authorList.Add(author6);
            
            return authorList;
        }
        private IEnumerable<Book> BuildBookList()
        {
            var bookList = new List<Book>();
            var book1 = new Book()
            {
                Book_title = "Przebudzenie",
                Page_count = 456,
                RealeseDate = new DateTime(1990, 1, 1),
            };
            bookList.Add(book1);
            
            var book2 = new Book()
            {
                Book_title = "Świat według Clarsona",
                Page_count = 214,
                RealeseDate = new DateTime(1990, 1, 1),
            };
            bookList.Add(book2);
            
            var book3 = new Book()
            {
                Book_title = "Filozofia XD",
                Page_count = 268,
                RealeseDate = new DateTime(2020, 1, 1),
            };
            bookList.Add(book3);
            
            var book4 = new Book()
            {
                Book_title = "Tajemniczy opiekun",
                Page_count = 105,
                RealeseDate = new DateTime(1990, 1, 1),
            };
            bookList.Add(book4);
            
            var book5 = new Book()
            {
                Book_title = "Posiew ocalenia..",
                Page_count = 556,
                RealeseDate = new DateTime(1990, 1, 1),
            };
            bookList.Add(book5);
            
            var book6 = new Book()
            {
                Book_title = "Lalka",
                Page_count = 456,
                RealeseDate = new DateTime(1990, 1, 1),
            };
            bookList.Add(book6);
            return bookList;
        }
        private IEnumerable<Category> BuildCategoryList()
        {
            var categoryList = new List<Category>();
            var category1 = new Category()
            {
                Category_name = "Horror",
            };
            categoryList.Add(category1);
            
            var category2 = new Category()
            {
                Category_name = "Felietony",
            };
            categoryList.Add(category2);
            
            var category3 = new Category()
            {
                Category_name = "Powiesc romantyczna",
            };
            categoryList.Add(category3);
            
            var category4 = new Category()
            {
                Category_name = "Poezja",
            };
            categoryList.Add(category4);
            
            var category5 = new Category()
            {
                Category_name = "Powieść społeczno-obyczajowa",
            };
            categoryList.Add(category5);
            return categoryList;
        }

        private IEnumerable<Book_author> BuildBookAuthorList(IEnumerable<Book> bookList, IEnumerable<Author> authorList)
        {
            var bookAuthorList = new List<Book_author>();
            
            for (int i = 0; i < bookList.Count(); i++)
            {
                var loopBook_author = new Book_author()
                {
                Author_id = authorList.ToList()[i].Author_id,
                Book_id = bookList.ToList()[i].Book_id,
                };
                bookAuthorList.Add(loopBook_author);
            }
            return bookAuthorList;
        }
        private IEnumerable<Book_category> BuildBookCategoryList(IEnumerable<Book> bookList, IEnumerable<Category> categoryList)
        {
            var bookCategoryList = new List<Book_category>();
           
            for (int i = 0; i < categoryList.Count(); i++)
            {
                var loopBook_category = new Book_category()
                {
                    Book_id = bookList.ToList()[i].Book_id,
                    Category_id = categoryList.ToList()[i].Category_id,
                };
                bookCategoryList.Add(loopBook_category);
            }
            
            return bookCategoryList;
        }

        private IEnumerable<Opinion> BuildOpinionList(IEnumerable<User> userList, IEnumerable<Book> bookList)
        {
            var opinionList = new List<Opinion>();
            var counter = 0;
            Random rand = new Random();
            foreach (var user in userList)
            {
                var bookId = rand.Next(bookList.Count());
                counter++;
                opinionList.Add(new Opinion ()
                    {
                        User_id = user.User_id,
                       Book_id = bookList.ToList()[bookId].Book_id,
                       Oponion_body = "body",
                       Opinion_date = DateTime.Now,
                       IsActiveComment = true,
                    });
            }
            return opinionList;
        }
    }
}