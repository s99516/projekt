﻿namespace DBApp.Common.Enums
{
    public enum Gender
    {
        Male = 0,
        Female = 1
    }
}