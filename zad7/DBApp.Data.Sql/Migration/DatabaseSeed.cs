﻿using System;
using System.Collections.Generic;
using System.Linq;
using DBApp.Common.Enums;
using DBApp.Common.Extensions;
using DBApp.Data.Sql.DAO;

namespace DBApp.Data.Sql.Migration
{
    public class DatabaseSeed
    {
        private readonly DBAppDBContext _context;

        public DatabaseSeed(DBAppDBContext context)
        {
            _context = context;
        }

        public void Seed()
        {
            var userList = BuildUserList();
            //dodanie użytkowników do tabeli User
            _context.User.AddRange(userList);
            //zapisanie zmian w bazie danych
            _context.SaveChanges();

            var filmList = BuildFilmList();
            _context.Film.AddRange(filmList);
            _context.SaveChanges();


            var commentList = BuildCommentList(filmList, userList);
            _context.Comment.AddRange(commentList);
            _context.SaveChanges();

            var ratingList = BuildRatingList(filmList, userList);
            _context.Rating.AddRange(ratingList);
            _context.SaveChanges();

            var orderList = BuildOrderList(userList);
            _context.Order.AddRange(orderList);
            _context.SaveChanges();

            var filmOrderList = BuildFilmOrderList(filmList, orderList);
            _context.FilmOrder.AddRange(filmOrderList);
            _context.SaveChanges();


            var categoryList = BuildCategoryList();
            _context.Category.AddRange(categoryList);
            _context.SaveChanges();

            var filmCategoryList = BuildFilmCategoryList(filmList, categoryList);
            _context.FilmCategory.AddRange(filmCategoryList);
            _context.SaveChanges();
        }

        private IEnumerable<DAO.User> BuildUserList()
        {
            var userList = new List<DAO.User>();
            var user = new DAO.User
            {
                UserName = "Rafal",
                Email = "ra.zajac@student.po.edu.pl",
                RegistrationDate = DateTime.Now.AddYears(-3),
                EditionDate = DateTime.Now.AddYears(-3),
                BirthDate = new DateTime(2000, 2, 8),
                Gender = Gender.Male,
                IsActiveUser = true,
                IsBannedUser = false,
                AccountDescription = "desc",
                AccountPrivateHref = "http://we.po.opole.pl/",
                IconHref =
                    "https://www.bhphotovideo.com/images/images500x500/LEE_Filters_115R_Peacock_Blue_Color_Effect_688218.jpg",
                ThumbnailHref =
                    "https://www.bhphotovideo.com/images/images500x500/LEE_Filters_115R_Peacock_Blue_Color_Effect_688218.jpg"
            };
            userList.Add(user);

            var user2 = new DAO.User
            {
                UserName = "Krokodyl",
                Email = "krokodyl@student.po.edu.pl",
                RegistrationDate = DateTime.Now.AddYears(-2),
                EditionDate = DateTime.Now.AddYears(-2),
                BirthDate = new DateTime(1994, 6, 7),
                Gender = Gender.Male,
                IsActiveUser = true,
                IsBannedUser = false,
                AccountDescription = "super desc",
                AccountPrivateHref = "http://we.po.opole.pl/",
                IconHref =
                    "https://www.bhphotovideo.com/images/images500x500/LEE_Filters_115R_Peacock_Blue_Color_Effect_688218.jpg",
                ThumbnailHref =
                    "https://www.bhphotovideo.com/images/images500x500/LEE_Filters_115R_Peacock_Blue_Color_Effect_688218.jpg"
            };
            userList.Add(user2);

            for (var i = 3; i <= 20; i++)
            {
                var user3 = new DAO.User
                {
                    UserName = "user" + i,
                    Email = "user" + i + "@student.po.edu.pl",
                    RegistrationDate = DateTime.Now.AddYears(-2),
                    EditionDate = DateTime.Now.AddYears(-2),
                    BirthDate = new DateTime(1994, 6, 7),
                    Gender = i % 2 == 0 ? Gender.Female : Gender.Male,
                    IsActiveUser = true,
                    AccountDescription = "super desc",
                    AccountPrivateHref = "http://we.po.opole.pl/",
                    IconHref =
                        "https://upload.wikimedia.org/wikipedia/commons/thumb/2/21/Solid_black.svg/2000px-Solid_black.svg.png",
                    ThumbnailHref =
                        "https://upload.wikimedia.org/wikipedia/commons/thumb/2/21/Solid_black.svg/2000px-Solid_black.svg.png"
                };
                userList.Add(user3);
            }

            return userList;
        }

        private IEnumerable<Film> BuildFilmList()
        {
            var filmList = new List<Film>();

            var film1 = new Film
            {
                FilmId = 1,
                FilmTitle = "Lsnienie",
                FilmDescription = "description1",
                Director = "Stanley Kubrick",
                TimeInMinutes = 119
            };
            filmList.Add(film1);
            var film2 = new Film
            {
                FilmId = 2,
                FilmTitle = "Szklana pulapka 1",
                FilmDescription = "description2",
                Director = "John McTiernan",
                TimeInMinutes = 140
            };
            filmList.Add(film2);
            var film3 = new Film
            {
                FilmId = 3,
                FilmTitle = "Zielona Mila",
                FilmDescription = "description3",
                Director = "Frank Darabont",
                TimeInMinutes = 181
            };
            filmList.Add(film3);
            var film4 = new Film
            {
                FilmId = 4,
                FilmTitle = "Titanic",
                FilmDescription = "description4",
                Director = "James Cameron",
                TimeInMinutes = 194
            };
            filmList.Add(film4);
            var film5 = new Film
            {
                FilmId = 5,
                FilmTitle = "Skazani na Showshank",
                FilmDescription = "description5",
                Director = "Frank Darabont",
                TimeInMinutes = 142
            };
            filmList.Add(film5);
            return filmList;
        }

        private IEnumerable<Category> BuildCategoryList()
        {
            var categoryList = new List<Category>();

            var category1 = new Category
            {
                CategoryId = 1,
                CategoryName = "Science Fiction",
                ImageHref = "href1"
            };
            categoryList.Add(category1);
            var category2 = new Category
            {
                CategoryId = 2,
                CategoryName = "Horrror",
                ImageHref = "href2"
            };
            categoryList.Add(category2);
            var category3 = new Category
            {
                CategoryId = 3,
                CategoryName = "Dramat",
                ImageHref = "href3"
            };
            categoryList.Add(category3);
            var category4 = new Category
            {
                CategoryId = 4,
                CategoryName = "Film sensacyjny",
                ImageHref = "href4"
            };
            categoryList.Add(category3);
            return categoryList;
        }

        private IEnumerable<FilmCategory> BuildFilmCategoryList(IEnumerable<Film> filmList,
            IEnumerable<Category> categoryList)
        {
            var filmCategoryList = new List<FilmCategory>();
            filmList.ToList().Shuffle();
            categoryList.ToList().Shuffle();
            var rnd = new Random();
            for (var i = 0; i < filmList.Count(); i++)
            {
                var index = rnd.Next(0, categoryList.Count());

                filmCategoryList.Add(new FilmCategory
                {
                    CategoryId = categoryList.ToList()[index].CategoryId,
                    FilmId = filmList.ToList()[i].FilmId
                });
            }

            return filmCategoryList;
        }

        private IEnumerable<Order> BuildOrderList(IEnumerable<DAO.User> userList)
        {
            var i = 1;
            var orderList = new List<Order>();
            foreach (var user in userList)
            {
                if (user.UserName != "Rafal" && user.UserName != "Krokodyl")
                    orderList.Add(new Order
                    {
                        //OrderId = i,
                        UserId = user.UserId
                    });
                i++;
            }

            return orderList;
        }

        private IEnumerable<FilmOrder> BuildFilmOrderList(IEnumerable<Film> filmList,
            IEnumerable<Order> orderList)
        {
            var i = 1;
            var random = new Random();
            var FilmOrderList = new List<FilmOrder>();
            foreach (var order in orderList)
            {
                FilmOrderList.Add(new FilmOrder
                {
                    FilmOrderId = i,
                    FilmId = random.Next(1, 3),
                    OrderId = i
                });
                i++;
            }

            return FilmOrderList;
        }


        private IEnumerable<Comment> BuildCommentList(
            IEnumerable<Film> filmList,
            IEnumerable<DAO.User> userList)
        {
            var commentList = new List<Comment>();
            var counter = 0;
            var rand = new Random();
            var filmsCount = filmList.ToList().Count;
            foreach (var user in userList)
            {
                counter++;
                if (user.UserName != "Salama")
                {
                    var filmId = rand.Next(filmsCount);
                    commentList.Add(new Comment
                    {
                        FilmId = filmList.ToList()[filmId].FilmId,
                        UserId = user.UserId,
                        CommentBody = "comment" + counter,
                        CommentDate = DateTime.Now,
                        IsActiveComment = true,
                        IsBannedComment = false
                    });
                    filmList.ToList()[filmId].CommentsCount += 1;
                }
            }

            return commentList;
        }

        private IEnumerable<Rating> BuildRatingList(
            IEnumerable<Film> filmList,
            IEnumerable<DAO.User> userList)
        {
            var ratingList = new List<Rating>();
            var counter = 0;
            var rand = new Random();
            var filmsCount = filmList.ToList().Count;
            foreach (var user in userList)
            {
                counter++;
                if (user.UserName != "Salama")
                {
                    var filmId = rand.Next(filmsCount);
                    ratingList.Add(new Rating
                    {
                        FilmId = filmList.ToList()[filmId].FilmId,
                        UserId = user.UserId,
                        Rate = rand.Next(1, 5),
                        RateDate = DateTime.Now,
                        IsActiveRating = true,
                        IsBannedRating = false
                    });
                }
            }

            return ratingList;
        }
    }
}