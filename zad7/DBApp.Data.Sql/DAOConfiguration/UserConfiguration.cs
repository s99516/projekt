﻿//using DBApp.Data.Sql.DAO;

using DBApp.Data.Sql.DAO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace DBApp.Data.Sql.DAOConfiguration
{
    public class UserConfiguration : IEntityTypeConfiguration<DAO.User>
    {
        public void Configure(EntityTypeBuilder<DAO.User> builder)
        {
            builder.Property(x => x.UserName).IsRequired();
            builder.Property(x => x.Email).IsRequired();
            builder.Property(c => c.IsActiveUser).HasColumnType("tinyint(1)");
            builder.Property(c => c.IsBannedUser).HasColumnType("tinyint(1)");
            builder.ToTable("User");
        }
    }
}