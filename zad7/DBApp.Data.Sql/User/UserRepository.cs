﻿using System.Threading.Tasks;
using DBApp.IData.User;
using Microsoft.EntityFrameworkCore;

namespace DBApp.Data.Sql.User
{
    public class UserRepository : IUserRepository
    {
        private readonly DBAppDBContext _context;

        public UserRepository(DBAppDBContext context)
        {
            _context = context;
        }
        public async Task<int> AddUser(Domain.User.User user)
        {
            var userDAO = new DAO.User()
            {
                UserName = user.UserName,
                Email = user.Email,
                Gender = user.Gender,
                BirthDate = user.BirthDate,
                RegistrationDate = user.RegistrationDate,
                EditionDate = user.EditionDate,
                IsActiveUser = user.IsActiveUser,
                IsBannedUser = user.IsBannedUser
            };
            await _context.AddAsync(userDAO);
            await _context.SaveChangesAsync();
            return userDAO.UserId;
        }

        public async Task<Domain.User.User> GetUser(int userId)
        {
            var user = await _context.User.FirstOrDefaultAsync(x => x.UserId == userId);
            return new Domain.User.User(
                user.UserId,
                user.UserName,
                user.Email,
                user.RegistrationDate,
                user.EditionDate,
                user.UserInfoEditionDate,
                user.Gender,
                user.BirthDate,
                user.IsBannedUser,
                user.IsActiveUser,
                user.AccountDescription,
                user.AccountPrivateHref,
                user.IconHref,
                user.ThumbnailHref);
        }

        public async Task<Domain.User.User> GetUser(string userName)
        {
            var user = await _context.User.FirstOrDefaultAsync(x => x.UserName == userName);
            return new Domain.User.User(
                user.UserId,
                user.UserName,
                user.Email,
                user.RegistrationDate,
                user.EditionDate,
                user.UserInfoEditionDate,
                user.Gender,
                user.BirthDate,
                user.IsBannedUser,
                user.IsActiveUser,
                user.AccountDescription,
                user.AccountPrivateHref,
                user.IconHref,
                user.ThumbnailHref);
        }

        public async Task EditUser(Domain.User.User user)
        {
            var editUser = await _context.User.FirstOrDefaultAsync(x => x.UserId == user.UserId);
            editUser.UserName = user.UserName;
            editUser.Email = user.Email;
            editUser.Gender = user.Gender;
            editUser.BirthDate = user.BirthDate;
            editUser.EditionDate = user.EditionDate;
            await _context.SaveChangesAsync();
        }
    }
}