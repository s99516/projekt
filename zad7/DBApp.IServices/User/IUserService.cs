﻿using System.Threading.Tasks;
using DBApp.IServices.Request;

namespace DBApp.IServices.User
{
    public interface IUserService
    {
        Task<DBApp.Domain.User.User> GetUserByUserId(int userId);
        Task<DBApp.Domain.User.User> GetUserByUserName(string userName);
        Task<DBApp.Domain.User.User> CreateUser(CreateUser createUser);
        Task EditUser(EditUser createUser, int userId);
        //Task DeleteUser(int userId);
    }
}