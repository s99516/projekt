﻿using System;
using DBApp.Common.Enums;

namespace DBApp.IServices.Request
{
    public class EditUser
    {
        public string UserName { get; set; }
        public string Email { get; set; }
        public Gender Gender { get; set; }
        public DateTime BirthDate { get; set; }
    }
}