﻿using DBApp.Api.ViewModels;
using Org.BouncyCastle.Pkcs;

namespace DBApp.Api.Mappers
{
    public class UserToUserViewModelMapper
    {
        public static UserViewModel UserToUserViewModel(Domain.User.User user)
        {
            var userViewModel = new UserViewModel()
            {
                UserName = user.UserName,
                UserId = user.UserId,
                Email = user.Email,
                Gender = user.Gender,
                BirthDate = user.BirthDate,
                EditionDate = user.EditionDate,
                RegistrationDate = user.RegistrationDate,
                IconHref = user.IconHref,
                ThumbnailHref = user.ThumbnailHref,
                AccountDescription = user.AccountDescription,
                UserInfoEditionDate = user.UserInfoEditionDate
            };
            return userViewModel;
        }
    }
}