﻿using System;

namespace DBApp.Domain.DomainExceptions
{
    public class InvalidBirthDateException : Exception
    {
        public InvalidBirthDateException(DateTime birthDate) : base(ModifyMessage(birthDate))
        {
        }

        public static string ModifyMessage(DateTime birthDate)
        {
            return $"Invalid birth date {birthDate}";
        }
    }
}