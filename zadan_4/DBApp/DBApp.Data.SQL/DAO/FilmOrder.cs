﻿namespace DBApp.Data.SQL.DAO
{
    public class FilmOrder
    {
        public int FilmOrderId { get; set; }
        public int FilmId { get; set; }
        public int OrderId { get; set; }
        
        public virtual Film Film { get; set; }
        public virtual Order Order { get; set; }
    }
}