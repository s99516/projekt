﻿using System.Collections.Generic;

namespace DBApp.Data.SQL.DAO
{
    public class Film
    {
        public Film()
        {
            Comments = new List<Comment>();
            FilmOrders = new List<FilmOrder>();
            FilmCategories = new List<FilmCategory>();
            Ratings = new List<Rating>();
        }
        
        public  int FilmId { get; set; }
        
        public string FilmTitle { get; set; }
        public string FilmDescription { get; set; }
        public string Director { get; set; }
        public int TimeInMinutes { get; set; }
        public int CommentsCount { get; set; }
        

        //public virtual Order Order { get; set; }
        public virtual ICollection<Comment> Comments { get; set; }
        public virtual ICollection<FilmOrder> FilmOrders { get; set; }
        public virtual ICollection<FilmCategory> FilmCategories { get; set; }
        public virtual ICollection<Rating> Ratings { get; set; }
    }
}