﻿using System;

namespace DBApp.Data.SQL.DAO
{
    public class Rating
    {
        public int RatingId { get; set; }
        public int FilmId { get; set; }
        public int UserId { get; set; }
        public int Rate { get; set; }
        public DateTime RateDate { get; set; }
        public bool IsActiveRating { get; set; }
        public bool IsBannedRating { get; set; }
        
        public virtual Film Film { get; set; }
        public virtual User User { get; set; }
    }
}