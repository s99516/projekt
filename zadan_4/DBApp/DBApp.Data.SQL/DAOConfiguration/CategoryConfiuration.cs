﻿using DBApp.Data.SQL.DAO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DBApp.Data.SQL.DAOConfiguration
{
    public class CategoryConfiuration : IEntityTypeConfiguration<Category>
    {
        public void Configure(EntityTypeBuilder<Category> builder)
        {
            builder.Property(x => x.CategoryName).IsRequired();
            builder.Property(x => x.ImageHref).IsRequired();

            builder.ToTable("Category");
        }
    }
}