﻿using DBApp.Data.SQL.DAO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DBApp.Data.SQL.DAOConfiguration
{
    public class FilmConfiguration :IEntityTypeConfiguration<Film>
    {
        public void Configure(EntityTypeBuilder<Film> builder)
        {
            builder.Property(x => x.FilmDescription).IsRequired();
            builder.Property(x => x.TimeInMinutes).IsRequired();

            builder.ToTable("Film");


        }
    }
}