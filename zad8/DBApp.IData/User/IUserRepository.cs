﻿using System.Threading.Tasks;

namespace DBApp.IData.User
{
    public interface IUserRepository
    {
        Task<int> AddUser(DBApp.Domain.User.User user);
        Task<DBApp.Domain.User.User> GetUser(int userId);
        Task<DBApp.Domain.User.User> GetUser(string userName);
        Task EditUser(Domain.User.User user);
    }
}