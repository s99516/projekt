﻿using System;
using System.Threading.Tasks;
using DBApp.Common.Enums;
using DBApp.Domain.DomainExceptions;
using DBApp.IData.User;
using DBApp.IServices.Request;
using DBApp.IServices.User;
using DBApp.Services.User;
using Moq;
using Xunit;

namespace DBApp.Services.Tests.User
{
    public class UserServiceTest
    {
        private readonly IUserService _userService;
        private readonly Mock<IUserRepository> _userRepositoryMock;

        public UserServiceTest()
        {
            _userRepositoryMock = new Mock<IUserRepository>();
            _userService = new UserService(_userRepositoryMock.Object);
        }

        [Fact]
        public void CreateUserReturnsthrowsInvalidBirthDateException()
        {
            var user = new CreateUser()
            {
                Email = "Email",
                Gender = Gender.Male,
                UserName = "Username",
                BirthDate = DateTime.UtcNow.AddHours(1)
            };
            Assert.ThrowsAsync<InvalidBirthDateException>(() => _userService.CreateUser(user));
        }
        
        [Fact]
        public async Task CreateUserReturnsCorrectResponse()
        {
            var user = new CreateUser()
            {
                Email = "Email",
                Gender = Gender.Male,
                UserName = "Username",
                BirthDate = DateTime.UtcNow
            };
            int expecrResult = 0;
            _userRepositoryMock.Setup(x => x.AddUser(
                new Domain.User.User(
                    user.UserName,
                    user.Email,
                    user.Gender,
                    user.BirthDate))).Returns(Task.FromResult(expecrResult));

            var result = await _userService.CreateUser(user);

            Assert.IsType<Domain.User.User>(result);
            Assert.NotNull(result);
            Assert.Equal(expecrResult,result.UserId);
        }
        
    }
}