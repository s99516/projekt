﻿using System;
using System.Threading.Tasks;
using DBApp.Common.Enums;
using DBApp.Domain.DomainExceptions;
using Xunit;

namespace DBApp.Domain.Tests.User
{
    public class UserTest
    {
        public UserTest()
        {
            
        }

        [Fact]
        public void CreateUserReturnsThrowsInvalidBirthDateException()
        {
            Assert.Throws<InvalidBirthDateException>
            (
                () => new Domain.User.User("Name", "Email", Gender.Male, DateTime.UtcNow.AddHours(1))
            );
        }

        [Fact]
        public void CreateuserReturnsCorrectResponse()
        {
            var dateTime = DateTime.UtcNow;
            var user = new Domain.User.User("UserName", "UserEmail", Gender.Male, dateTime);
            
            Assert.Equal("UserName", user.UserName);
            Assert.Equal("UserEmail", user.Email);
            Assert.Equal(Gender.Male, user.Gender);
            Assert.Equal(dateTime, user.BirthDate);
        }
        
        [Fact]
        public void CreateUserReturnsIncorrectResponse()
        {
            var dateTime = DateTime.UtcNow;
            var user = new Domain.User.User("UserName1", "UserEmail1", Gender.Female, dateTime);
            
            Assert.NotEqual(true, user.IsBannedUser);
            Assert.NotEqual(false, user.IsActiveUser);
        }
        
    }
}