﻿using DBApp.Data.Sql.DAO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DBApp.Data.Sql.DAOConfiguration
{
    public class FilmOrderConfiguration : IEntityTypeConfiguration<FilmOrder>
    {
        public void Configure(EntityTypeBuilder<FilmOrder> builder)
        {
            builder.HasOne(x => x.Order)
                .WithMany(x => x.FilmOrders)
                .OnDelete(DeleteBehavior.Restrict)
                .HasForeignKey(x => x.OrderId);

            builder.HasOne(x => x.Film)
                .WithMany(x => x.FilmOrders)
                .OnDelete(DeleteBehavior.Restrict)
                .HasForeignKey(x => x.FilmId);

            builder.ToTable("FilmOrder");
        }
    }
}