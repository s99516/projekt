﻿using System;
using System.Collections.Generic;
using DBApp.Common.Enums;

namespace DBApp.Data.Sql.DAO
{
    public class User
    {
        public User()
        {
            Comments = new List<Comment>();
            FilmsOrders = new List<FilmOrder>();
            Orders = new List<Order>();
            Ratings = new List<Rating>();
        }

        public int UserId { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public DateTime RegistrationDate { get; set; }
        public DateTime EditionDate { get; set; }
        public DateTime UserInfoEditionDate { get; set; }
        public Gender Gender { get; set; }
        public DateTime BirthDate { get; set; }
        public bool IsBannedUser { get; set; }
        public bool IsActiveUser { get; set; }
        public string AccountDescription { get; set; }
        public string AccountPrivateHref { get; set; }
        public string IconHref { get; set; }
        public string ThumbnailHref { get; set; }

        public virtual ICollection<Order> Orders { get; set; }
        public virtual ICollection<FilmOrder> FilmsOrders { get; set; }
        public virtual ICollection<Comment> Comments { get; set; }
        public virtual ICollection<Rating> Ratings { get; set; }
    }
}