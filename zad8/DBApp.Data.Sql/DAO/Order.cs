﻿using System.Collections.Generic;

namespace DBApp.Data.Sql.DAO
{
    public class Order
    {
        public Order()
        {
            FilmOrders = new List<FilmOrder>();
        }

        public int OrderId { get; set; }
        public int UserId { get; set; }

        public virtual User User { get; set; }
        public virtual ICollection<FilmOrder> FilmOrders { get; set; }
    }
}