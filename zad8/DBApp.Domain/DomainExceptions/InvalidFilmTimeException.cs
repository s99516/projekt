﻿using System;

namespace DBApp.Domain.DomainExceptions
{
    public class InvalidFilmTimeException : Exception
    {
        public InvalidFilmTimeException(int TimeInMinutes) : base(ModifyMessage(TimeInMinutes))
        {
            
        }
        public static string ModifyMessage(int TimeInMinutes)
        {
            return $"Invalid film time {TimeInMinutes}";
        }
    }
}