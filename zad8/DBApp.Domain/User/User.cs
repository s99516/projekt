﻿using System;
using DBApp.Common.Enums;
using DBApp.Domain.DomainExceptions;

namespace DBApp.Domain.User
{
    public class User
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public DateTime RegistrationDate { get; set; }
        public DateTime EditionDate { get; set; }
        public DateTime UserInfoEditionDate { get; set; }
        public Gender Gender { get; set; }
        public DateTime BirthDate { get; set; }
        public bool IsBannedUser { get; set; }
        public bool IsActiveUser { get; set; }
        public string AccountDescription { get; set; }
        public string AccountPrivateHref { get; set; }
        public string IconHref { get; set; }
        public string ThumbnailHref { get; set; }

        public User(int id, string userName, string email, 
            DateTime creatDate, DateTime editDate, 
            DateTime userInfoEditDate, Gender gender, 
            DateTime birthDate, bool isBannedUser, 
            bool isActiveUser, string accountDescription, 
            string accountPrivateHref, string iconHref, 
            string thumbnailHref)
        {
            UserId = id;
            UserName = userName;
            Email = email;
            RegistrationDate = creatDate;
            EditionDate = editDate;
            UserInfoEditionDate = userInfoEditDate;
            Gender = gender;
            IsBannedUser = isBannedUser;
            IsActiveUser = isActiveUser;
            AccountDescription = accountDescription;
            AccountPrivateHref = accountPrivateHref;
            IconHref = iconHref;
            ThumbnailHref = thumbnailHref;
        }

        public User(string userName, string email, Gender gender, DateTime birthDate)
        {
            if (birthDate >= DateTime.UtcNow)
            {
                throw new InvalidBirthDateException(birthDate);
            }
            UserName = userName;
            Email = email;
            Gender = gender;
            BirthDate = birthDate;
            RegistrationDate = DateTime.UtcNow;
            EditionDate = DateTime.UtcNow;
            IsBannedUser = false;
            IsActiveUser = true;
        }

        public void EditUser(string userName, string email, Gender gender, DateTime birthDate)
        {
            if (birthDate >= DateTime.UtcNow)
            {
                throw new InvalidBirthDateException(birthDate);
            }
            UserName = userName;
            Email = email;
            Gender = gender;
            BirthDate = birthDate;
            EditionDate = DateTime.UtcNow;
        }
    }
}