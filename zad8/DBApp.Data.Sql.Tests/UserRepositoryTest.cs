﻿using System;
using System.Threading.Tasks;
using DBApp.Common.Enums;
using DBApp.Data.Sql.User;
using DBApp.Domain.DomainExceptions;
using DBApp.IData.User;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestPlatform.ObjectModel.Host;
using Xunit;

namespace DBApp.Data.Sql.Tests

{
    public class UserRepositoryTest
    {
        public IConfiguration Configuration { get; set; }
        private DBAppDBContext _context;
        private IUserRepository _userRepository;

        public UserRepositoryTest()
        {
            var optionsBuilder = new DbContextOptionsBuilder<DBAppDBContext>();
            optionsBuilder.UseMySQL("server=localhost;userid=root;pwd=rafal69;port=3306;database=zad8_database;");
            _context = new DBAppDBContext(optionsBuilder.Options);
            _context.Database.EnsureDeleted();
            _context.Database.EnsureCreated();
            _userRepository = new UserRepository(_context);
        }
        [Fact]

        public async Task AddUserReturnsCorrectResponse()
        {
            var user = new Domain.User.User("Name", "Email", Gender.Male, DateTime.UtcNow);
            var userId = await _userRepository.AddUser(user);
            var createdUser = await _context.User.FirstOrDefaultAsync(x => x.UserId == userId);
            Assert.NotNull(createdUser);

            _context.User.Remove(createdUser);
            await _context.SaveChangesAsync();
        }
        [Fact]

        public async Task AddUserThrowsInvalidDateBirthException()
        {
            
            Assert.ThrowsAsync<InvalidBirthDateException>
            ( async () =>
                {
                    
                    var user = new Domain.User.User("Name", "Email", Gender.Male, DateTime.UtcNow.AddHours(1));
                    var userId =   await _userRepository.AddUser(user);
                    var createdUser = await  _context.User.FirstOrDefaultAsync(x => x.UserId == userId);
                    _context.User.Remove(createdUser);
                }
            );
            await _context.SaveChangesAsync();
        }
    }
}