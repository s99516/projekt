﻿using System;
using System.Threading.Tasks;
using DBApp.Api.BindingModels;
using DBApp.Api.Validation;
using DBApp.Api.ViewModels;
using DBApp.Data.Sql;
using DBApp.Data.Sql.DAO;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace DBApp.Api.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    public class UserController : Controller
    {
        private readonly DBAppDBContext _context;

        public UserController(DBAppDBContext context)
        {
            _context = context;
        }

        [HttpGet("{userId:min(1)}", Name = "GetUserById")]
        public async Task<IActionResult> GetUserByID(int userId)
        {
            var user = await _context.User.FirstOrDefaultAsync(x => x.UserId == userId);

            if (user != null)
            {
                return Ok(new UserViewModel
                {
                    UserId = user.UserId,
                    Email = user.Email,
                    Gender = user.Gender,
                    UserName = user.UserName,
                    BirthDate = user.BirthDate,
                    EditionDate = user.EditionDate,
                    RegistrationDate = user.RegistrationDate,
                    IconHref = user.IconHref,
                    ThumbnailHref = user.IconHref,
                    AccountDescription = user.AccountDescription,
                    UserInfoEditionDate = user.UserInfoEditionDate
                });
            }
            return NotFound();
        }

        [HttpGet("name/{userName}", Name = "GetUserByUserName")]
        public async Task<IActionResult> GetUserByUserName(string userName)
        {
            var user = await _context.User.FirstOrDefaultAsync(x => x.UserName == userName);

            if (user != null)
            {
                return Ok(new UserViewModel
                {
                    UserId = user.UserId,
                    Email = user.Email,
                    Gender = user.Gender,
                    UserName = user.UserName,
                    BirthDate = user.BirthDate,
                    EditionDate = user.EditionDate,
                    RegistrationDate = user.RegistrationDate,
                    IconHref = user.IconHref,
                    ThumbnailHref = user.IconHref,
                    AccountDescription = user.AccountDescription,
                    UserInfoEditionDate = user.UserInfoEditionDate
                });
            }

            return NotFound();
        }

        [ValidateModel]
        public async Task<IActionResult> Post([FromBody] CreateUser createUser)
        {
            var user = new User
            {
                Email = createUser.Email,
                UserName = createUser.UserName,
                Gender = createUser.Gender,
                BirthDate = createUser.BirthDate,
                RegistrationDate = DateTime.UtcNow,
                EditionDate = DateTime.UtcNow,
                IsActiveUser = true,
                IsBannedUser = false
            };
            await _context.AddAsync(user);
            await _context.SaveChangesAsync();

            return Created(user.UserId.ToString(), new UserViewModel
            {
                UserId = user.UserId,
                Email = user.Email,
                Gender = user.Gender,
                UserName = user.UserName,
                BirthDate = user.BirthDate,
                EditionDate = user.EditionDate,
                RegistrationDate = user.RegistrationDate,
                IconHref = user.IconHref,
                ThumbnailHref = user.IconHref,
                AccountDescription = user.AccountDescription,
                UserInfoEditionDate = user.UserInfoEditionDate
            });
        }

        [ValidateModel]
        [HttpPatch("edit/{userId:min(1)}",Name = "EditUser")]
        public async Task<IActionResult> EditUser([FromBody] EditUser editUser, int userId)
        {
            var user = await _context.User.FirstOrDefaultAsync(x => x.UserId == userId);
            user.UserName = editUser.UserName;
            user.Email = editUser.Email;
            user.BirthDate = editUser.BirthDate;
            user.Gender = editUser.Gender;
            await _context.SaveChangesAsync();
            return NoContent();
        }
        

        //[ValidateModel]
        [HttpDelete("delete/{userId:min(1)}", Name = "DeleteUser")]
        public async Task<IActionResult> DeleteUser(int userId)
        {
            var user = await _context.User.FirstOrDefaultAsync(x => x.UserId == userId);
            if (user == null)
            {
                return NotFound();
            }
            else
            {
                var comment = await _context.Comment.FirstOrDefaultAsync(x => x.UserId == userId);
                while (comment != null)
                {
                    _context.Comment.Remove(comment);
                    await _context.SaveChangesAsync();
                    comment = await _context.Comment.FirstOrDefaultAsync(x => x.UserId == userId);
                }

                var order = await _context.Order.FirstOrDefaultAsync(x => x.UserId == userId);
                while (order != null)
                {
                    var filmOrder = await _context.FilmOrder.FirstOrDefaultAsync(x => x.OrderId == order.OrderId);
                    while (filmOrder != null)
                    {
                        _context.FilmOrder.Remove(filmOrder);
                        await _context.SaveChangesAsync();
                        filmOrder = await _context.FilmOrder.FirstOrDefaultAsync(x => x.OrderId == order.OrderId);
                    }

                    _context.Order.Remove(order);
                    await _context.SaveChangesAsync();
                    order = await _context.Order.FirstOrDefaultAsync(x => x.UserId == userId);
                }

                var rating = await _context.Rating.FirstOrDefaultAsync(x => x.UserId == userId);
                while (rating != null)
                {
                    _context.Rating.Remove(rating);
                    await _context.SaveChangesAsync();
                    rating = await _context.Rating.FirstOrDefaultAsync(x => x.UserId == userId);
                }

                _context.User.Remove(user);
                await _context.SaveChangesAsync();
                return Ok();
            }
        }
    }
}