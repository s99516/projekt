﻿using System.Threading.Tasks;
using DBApp.Api.Mappers;
using DBApp.Api.Validation;
using DBApp.Data.Sql;
using DBApp.IServices.User;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace DBApp.Api.Controllers
{
    [ApiVersion("2.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    public class User2Controller : Controller
    {
        private readonly DBAppDBContext _context;
        private readonly IUserService _userService;

        public User2Controller(DBAppDBContext context, IUserService userService)
        {
            _context = context;
            _userService = userService;
        }

        [HttpGet("{userId:min(1)}", Name = "GetUserById")]
        public async Task<IActionResult> GetUserById(int userId)
        {
            var user = await _userService.GetUserByUserId(userId);
            if (user != null)
            {
                return Ok(UserToUserViewModelMapper.UserToUserViewModel(user));
            }
            return NotFound();
        }

        [HttpGet("name/{userName}", Name = "GetUserByUserName")]
        public async Task<IActionResult> GetUserByUserName(string userName)
        {
            var user = await _userService.GetUserByUserName(userName);
            if (user != null)
            {
                return Ok(UserToUserViewModelMapper.UserToUserViewModel(user));
            }
            return NotFound();
        }
        [ValidateModel]

        public async Task<IActionResult> Post([FromBody] IServices.Request.CreateUser createUser)
        {
            var user = await _userService.CreateUser(createUser);
            return Created(user.UserId.ToString(), UserToUserViewModelMapper.UserToUserViewModel(user));
        }

        [ValidateModel]
        [HttpPatch("edit/{userId:min(1)}",Name = "EditUser")]
        public async Task<IActionResult> EditUser([FromBody] IServices.Request.EditUser editUser, int userId)
        {
            await _userService.EditUser(editUser, userId);
            return NoContent();
        }

        [HttpDelete("delete/{userId:min(1)}", Name = "DeleteUser")]
        public async Task<IActionResult> DeleteUser(int userId)
        {
            var user = await _context.User.FirstOrDefaultAsync(x => x.UserId == userId);
            if (user == null)
            {
                return NotFound();
            }
            else
            {
                var comment = await _context.Comment.FirstOrDefaultAsync(x => x.UserId == userId);
                while (comment != null)
                {
                    _context.Comment.Remove(comment);
                    await _context.SaveChangesAsync();
                    comment = await _context.Comment.FirstOrDefaultAsync(x => x.UserId == userId);
                }

                var order = await _context.Order.FirstOrDefaultAsync(x => x.UserId == userId);
                while (order != null)
                {
                    var filmOrder = await _context.FilmOrder.FirstOrDefaultAsync(x => x.OrderId == order.OrderId);
                    while (filmOrder != null)
                    {
                        _context.FilmOrder.Remove(filmOrder);
                        await _context.SaveChangesAsync();
                        filmOrder = await _context.FilmOrder.FirstOrDefaultAsync(x => x.OrderId == order.OrderId);
                    }

                    _context.Order.Remove(order);
                    await _context.SaveChangesAsync();
                    order = await _context.Order.FirstOrDefaultAsync(x => x.UserId == userId);
                }

                var rating = await _context.Rating.FirstOrDefaultAsync(x => x.UserId == userId);
                while (rating != null)
                {
                    _context.Rating.Remove(rating);
                    await _context.SaveChangesAsync();
                    rating = await _context.Rating.FirstOrDefaultAsync(x => x.UserId == userId);
                }

                _context.User.Remove(user);
                await _context.SaveChangesAsync();
                return Ok();
            }
        }
    }
}