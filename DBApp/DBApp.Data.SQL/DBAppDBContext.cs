﻿using DBApp.Data.SQL.DAO;
using DBApp.Data.SQL.DAOConfiguration;
using Microsoft.EntityFrameworkCore;

namespace DBApp.Data.SQL
{
    public class DBAppDBContext : DbContext
    {
        public DBAppDBContext(DbContextOptions<DBAppDBContext> options) : base(options) {}
        
        public virtual DbSet<Category> Category { get; set; }        
        public virtual DbSet<User> User { get; set; }
        public virtual DbSet<Film> Film { get; set; }
        public virtual DbSet<Comment> Comment { get; set; }   
        public virtual DbSet<FilmOrder> FilmOrder { get; set; }        
        public virtual DbSet<Order> Order { get; set; }        
        public virtual DbSet<Rating> Rating { get; set; }        
        public virtual DbSet<FilmCategory> FilmCategory { get; set; }

        //Przykład konfiguracji modeli/encji poprzez klasy konfiguracyjne z folderu DAOConfigurations
        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfiguration(new CategoryConfiuration());
            builder.ApplyConfiguration(new UserConfiguration());
            //builder.ApplyConfiguration(new FilmConfiguration());
            builder.ApplyConfiguration(new CommentConfiguration());
            builder.ApplyConfiguration(new FilmConfiguration());
            builder.ApplyConfiguration(new FilmOrderConfiguration());
            builder.ApplyConfiguration(new OrderConfiguration());
            builder.ApplyConfiguration(new RatingConfiguration());
            builder.ApplyConfiguration(new FilmCategoryConfiguration());
        }
    }
    
}