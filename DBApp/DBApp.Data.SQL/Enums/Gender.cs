﻿namespace DBApp.Data.SQL.Enums
{
    public enum Gender
    {
        Male = 0,
        Female = 1
    }
}