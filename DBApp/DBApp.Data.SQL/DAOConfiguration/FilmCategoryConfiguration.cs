﻿using DBApp.Data.SQL.DAO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DBApp.Data.SQL.DAOConfiguration
{
    public class FilmCategoryConfiguration : IEntityTypeConfiguration<FilmCategory>
    {
        public void Configure(EntityTypeBuilder<FilmCategory> builder)
        {
            builder.HasOne(x => x.Film)
                .WithMany(x => x.FilmCategories)
                .OnDelete(DeleteBehavior.Restrict)
                .HasForeignKey(x => x.FilmId);

            builder.HasOne(x => x.Category)
                .WithMany(x => x.FilmCategories)
                .OnDelete(DeleteBehavior.Restrict)
                .HasForeignKey(x => x.CategoryId);

            builder.ToTable("FilmCategory");
        }
    }
}