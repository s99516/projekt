﻿using DBApp.Data.SQL.DAO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DBApp.Data.SQL.DAOConfiguration
{
    public class RatingConfiguration : IEntityTypeConfiguration<Rating>
    {
        public void Configure(EntityTypeBuilder<Rating> builder)
        {
            builder.Property(x => x.Rate).IsRequired();
            
            builder.Property(x =>x.IsActiveRating).HasColumnType("tinyint(1)");
            builder.Property(x => x.IsBannedRating).HasColumnType("tinyint(1)");

            builder.HasOne(x => x.Film)
                .WithMany(x => x.Ratings)
                .OnDelete(DeleteBehavior.Restrict)
                .HasForeignKey(x => x.FilmId);

            builder.HasOne(x => x.User)
                .WithMany(x => x.Ratings)
                .OnDelete(DeleteBehavior.Restrict)
                .HasForeignKey(x => x.UserId);

            builder.ToTable("Rating");
        }
    }
}