﻿using Google.Protobuf;

namespace DBApp.Data.SQL.DAO
{
    public class FilmCategory
    {
        public int FilmCategoryId { get; set; }
        public int FilmId { get; set; }
        public int CategoryId { get; set; }
        
        public virtual Film Film { get; set; }

        public virtual Category Category { get; set; }
    }
}